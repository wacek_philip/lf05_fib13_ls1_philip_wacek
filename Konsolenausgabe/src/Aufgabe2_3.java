
public class Aufgabe2_3 {

	public static void main(String[] args) {
		// Festlegung der benötigten Variablen  
        String F = "Fahrenheit";
		String C = "Celsius";
		
		String a1 = "-20";
		double a2 = -28.8889;
		
		String b1 = "-10";
		double b2 = -23.3333;
		
		String c1 = "0";
		double c2 = -17.7778;
		
		String d1 = "20";
		double d2 = -6.6667;
		
		String e1 = "30";
		double e2 = -1.1111;
		
		// Ausgabe von Fahrenheit, Celsius und der Trennungslinie 
		System.out.printf("%-12s|", F);						 
		System.out.printf("%10s\n", C);						
		System.out.println("-------------------------");		
		
		// Ausgabe von -20 und -28,89
		System.out.printf("%-12s|",a1);					 
		System.out.printf("%10.2f\n",a2);					
															
	    // Ausgabe von -10 und -23,33 
		System.out.printf("%-12s|",b1);					       
		System.out.printf("%10.2f\n",b2);					
		
		// Ausgabe von +0 und -17,78
		System.out.printf("+%-11s|",c1);					
		System.out.printf("%10.2f\n",c2);					
		
		// Ausgabe von +20 und -6,67  
		System.out.printf("+%-11s|",d1);					
		System.out.printf("%10.2f\n",d2);					
		
		// Ausgabe von +30 und -1,11
		System.out.printf("+%-11s|",e1);					        
		System.out.printf("%10.2f\n",e2);					
		}

}
