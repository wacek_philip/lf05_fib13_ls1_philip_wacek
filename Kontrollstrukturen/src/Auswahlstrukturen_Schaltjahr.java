import java.util.Scanner;

public class Auswahlstrukturen_Schaltjahr {

	public static void main(String[] args) {
		int jahreszahl;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Jahreszahl: ");
		jahreszahl = myScanner.nextInt();
		
		if (jahreszahl % 4 == 0) {
			System.out.println("Schaltjahr.");
		}
	
		else if(jahreszahl % 100 == 0) {
			System.out.println("Schaltjahr.");
		}
		
		else if(jahreszahl % 400 == 0) {
			System.out.println("Schaltjahr.");
	    }
		else {
			System.out.println("Kein Schaltjahr.");
		}
		
		
			

}
	
}