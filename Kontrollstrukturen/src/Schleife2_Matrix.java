 import java.util.Scanner;

public class Schleife2_Matrix {

	public static void main(String[] args) {
		int eingabe;
		int i = 1;
		int j = 1;
		int zahl1;
		int zahl2;
		int zahl3;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		eingabe = myScanner.nextInt();
		System.out.print("0 ");
		
		if (eingabe >= 2 && eingabe <= 9) {
					while(i < 100 && j < 100) {
				zahl1 = i / 10;
				zahl2 = i % 10;
				zahl3 = zahl1 + zahl2;
				if(j % 10 == 0) {
					System.out.println(" ");
				}
			if(i % eingabe == 0) {
					System.out.print("* ");
				}
			else if(zahl3  == eingabe) {
					System.out.print("* ");
				}
			else if (i / 10 == eingabe) {
					System.out.print("* ");
				}
			else if (i % 10 == eingabe) {
					System.out.print("* ");
				}
			else {
					System.out.print(i + " ");
				}
				i = i + 1;
				j = j + 1;
			}
		
		}
	}

}
