import java.util.Scanner;

public class Auswahlstrukturen_BMI {

	public static void main(String[] args) {
		int gewicht; 
		double groesse;
		String geschlecht;
		double summe;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie Ihren Gewicht ein: ");
		gewicht = myScanner.nextInt();
		System.out.println("Bitte geben Sie Ihre Körpergröße in cm ein: ");
		groesse = myScanner.nextDouble();
		System.out.println("Bitte geben Sie Ihr Geschlecht ein: ");
		geschlecht = myScanner.next();
		
		groesse = groesse / 100;
		
		System.out.print("Ihr BMI beträgt: ");
		summe = (bmi(gewicht, groesse));
		System.out.println(summe);

		if (geschlecht.equals("m")) {
			if (summe < 20) {
				System.out.println("Sie haben Untergewicht.");
			}
			else if (summe >= 20 && summe <= 25) {
				System.out.println("Sie haben Normalgewicht.");
			}
			else {
				System.out.println("Sie haben Übergewicht.");
			}
		}
		
		if (geschlecht.equals("w")) {
			if (summe < 19) {
				System.out.println("Sie haben Untergewicht.");
			}
			else if (summe >= 19 && summe <= 24) {
				System.out.println("Sie haben Normalgewicht.");
			}
			else {
				System.out.println("Sie haben Übergewicht.");
			}
		}
	}
	static double bmi(double a, double b) {
		double bmi = (a / (b * b));
		return bmi;
	}

}
