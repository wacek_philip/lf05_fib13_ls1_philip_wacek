import java.util.Scanner;

public class Fallunterscheidungen_Rechner {

	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		String eingabe;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie die erste zahl ein: ");
		zahl1 = myScanner.nextDouble();
		System.out.println("Bitte geben Sie die Rechenoperation ein. Zur Verf�gung sind +, -, *, /");
		eingabe = myScanner.next();
		System.out.println("Bitte geben Sie die zweite Zahl ein: ");
		zahl2 = myScanner.nextDouble();
		
		switch (eingabe) {
		case "+":
			System.out.println(zahl1 + zahl2);
			break;
		case "-":
			System.out.println(zahl1 - zahl2);
			break;
		case "*":
			System.out.println(zahl1 * zahl2);
			break;
		case "/":
			System.out.println(zahl1 / zahl2);
			break;
		default:
			System.out.println("Die Eingabe war ung�ltig.");
		}
	}

}
