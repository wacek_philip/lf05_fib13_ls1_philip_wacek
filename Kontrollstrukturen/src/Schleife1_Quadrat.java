import java.util.Scanner;

public class Schleife1_Quadrat {

	public static void main(String[] args) {
		int eingabe;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl f�r das Quadrat ein: ");
		eingabe = myScanner.nextInt();
		for(int i = 1; i <= eingabe; i++) {
			for(int j = 1; j <= eingabe; j++) {
				System.out.print("* ");
			}
			System.out.println(" ");
		}
	}

}
