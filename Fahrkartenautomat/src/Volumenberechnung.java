
public class Volumenberechnung {

	public static void main(String[] args) {
		int breiteW�rfel = 15;
		int l�ngeW�rfel = 15; 
		int h�heW�rfel = 15;
		
		int breiteQuader = 16;
		int l�ngeQuader = 23;
		int h�heQuader = 17;
		
		int breitePyramide = 23;
		int l�ngePyramide = 18;
		int h�hePyramide = 4;
		
		int radiusKugel = 4;
		
		int multiW�rfel; 
		int multiQuader;
		int multiPyramide;
		double multiKugel;
		
		multiW�rfel = W�rfel(breiteW�rfel,l�ngeW�rfel,h�heW�rfel);
		multiQuader = Quader(breiteQuader,l�ngeQuader,h�heQuader);
		multiPyramide = Pyramide(breitePyramide,l�ngePyramide,h�hePyramide);
		multiKugel = Kugel(radiusKugel);
		
		System.out.println("Das Volumen des W�rfels betr�gt: " + multiW�rfel + " Kubikzentimeter");
		
		System.out.println("Das Volumen des Quaders betr�gt: " + multiQuader + " Kubikzentimeter");

		System.out.println("Das Volunen der Pyramide betr�gt: " + multiPyramide + " Kubikzentimeter");
		
		System.out.printf("Das Volumen der Kugel betr�gt: %.2f Kubikzentimeter", multiKugel);
		
	}
	public static int W�rfel(int x, int y, int z){
		return x * y * z;
	}
	
	public static int Quader(int x1, int y1, int z1){
		return x1 * y1 * z1;
	}
	public static int Pyramide(int x2, int y2, int z2){
		return x2 * y2 * z2 / 3;
	}
	public static double Kugel(double x3){
		return 4 / 3 * (x3 * x3 * x3) * Math.PI; 
	}
}

