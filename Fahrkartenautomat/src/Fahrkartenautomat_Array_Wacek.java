import java.util.Scanner;

class Fahrkartenautomat_Array_Wacek {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		String option;
		Scanner op = new Scanner(System.in);

		do {
			// Auslagerung der Kernstruktur des Fahrkartenautomats in Methoden
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n");

			// Soll ein weiterer durchlauf durchgef�hrt werden?
			System.out.println("Wollen Sie einen erneuten Durchlauf starten? (J/N)");
			option = op.next();

		} while (option.contains("J") || option.contains("j"));

		System.out.println(" >>> Programm beendet <<< ");
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		boolean bezahlen = false;
		byte ticketAuswahl;
		byte ticketAnzahl = 0;
		double gesamtpreis = 0.0;
		
		/* Aufgabe 1: Vorteil der 2 Array Methode:
		 *
		 *	Beschreibung[i] = Preis[i] -> man hat eine einfache Zugeh�rigkeit
		 *	zentrale Stelle im Code an der man Anpassungen vornehmen kann
		 *	dynamisch => wenn man die Arrays von der Gr��e ver�ndert, funktioniert alles trotzdem
		 *
		 *  Aufgabe 3: Vergleichen Sie die neue Implementierung
		 *	
		 *	dynamisch => wenn man die Arrays von der Gr��e ver�ndert, funktioniert alles trotzdem
		 *  man wei� das in beiden Arrays an position[i] der Name und Preis zu finden ist
		 *	wenn die Arrays eine ungleiche Groe�e haben dann gibt es Fehler
		 *	bei gro�er Ticketanzahl kann es schnell un�bersichtlich werden an welcher stelle die Preise eingef�gt werden m�ssen
		 *		
		 */
		
		String[] ticketBeschreibung = {
				  "Einzelfahrschein Berlin AB"
				, "Einzelfahrschein Berlin BC" 
				, "Einzelfahrschein Berlin ABC"
				, "Kurzstrecke"
				, "Tageskarte Berlin AB"
				, "Tageskarte Berlin BC"
				, "Tageskarte Berlin ABC"
				, "Kleingruppen-Tageskarte Berlin AB"
				, "Kleingruppen-Tageskarte Berlin BC"
				, "Kleingruppen-Tageskarte Berlin ABC"};
		double[] ticketPreise = { 
				  2.90
				, 3.30
				, 3.60
				, 1.90
				, 8.60
				, 9.00
				, 9.60
				, 23.50
				, 24.30
				, 24.90};

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("");

		// Fahrkartenbestellungs loop
		do {
			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin aus: ");

			// Ausgabe der einzelnen Array Elemente + option zum bezahlen
			for(int i = 0; i < ticketPreise.length; i++) {
				System.out.printf("  %s [%.2f EUR] (%d)\n", ticketBeschreibung[i], ticketPreise[i], i+1);
			}
			System.out.printf("Bezahlen (%d)\n", ticketPreise.length+1);

			
			// Ticket ausw�hlen
			System.out.print("Ihre Wahl: ");
			ticketAuswahl = tastatur.nextByte();

			// Wenn man bezahlen ausw�hlt, bezahlen auf true setzen
			if (ticketAuswahl == ticketPreise.length+1) {
				bezahlen = true;
			}
			
			// Nur in range vom Array Auswahlm�glichkeiten anbieten 
			while (ticketAuswahl <= 0 || ticketAuswahl > ticketPreise.length+1) {
				System.out.println(" >>falsche Eingabe<< ");
				System.out.print("Ihre Wahl: ");
				ticketAuswahl = tastatur.nextByte();
			}
			
			// Nur Anzahl der Tickets ausw�hlen wenn wir nicht bezahlen wollen
			if (bezahlen != true) {
				System.out.print("Anzahl der Tickets: ");
				ticketAnzahl = tastatur.nextByte();
			}

			// Ticketanzahl darf nur zwischen 0 und 10 sein
			while (ticketAnzahl > 10 || ticketAnzahl < 0) {
				System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
				System.out.print("Anzahl der Tickets: ");
				ticketAnzahl = tastatur.nextByte();
			}

			// Nur zwischensumme und Preis bestimmen wenn wir ein Ticket ausgew�hlt haben
			if (bezahlen != true) {
				gesamtpreis += ticketPreise[ticketAuswahl-1] * ticketAnzahl;
				System.out.printf("Zwischensumme: %.2f �\n", gesamtpreis);
			}
			
			// Zeilenumbruch
			System.out.println("");
		} while (bezahlen != true);

		return gesamtpreis;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		String einheit = "Euro";
		muenzeAusgeben(r�ckgabebetrag, einheit);
	}

	// Methode zum warten einer gewissen Zeit in Millisekunden
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Ausgabe der Muenzen in eigener Methode
	public static void muenzeAusgeben(double betrag, String einheit) {
		if (betrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f %s \n", betrag, einheit);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (betrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.printf("2 %s \n", einheit);
				betrag -= 2.0;
			}
			while (betrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.printf("1 %s \n", einheit);
				betrag -= 1.0;
			}
			while (betrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.printf("0.50 %s \n", einheit);
				betrag -= 0.5;
			}
			while (betrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.printf("0.20 %s \n", einheit);
				betrag -= 0.2;
			}
			while (betrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.printf("0.10 %s \n", einheit);
				betrag -= 0.1;
			}
			while (betrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.printf("0.05 %s \n", einheit);
				betrag -= 0.05;
			}
		}
	}
}
