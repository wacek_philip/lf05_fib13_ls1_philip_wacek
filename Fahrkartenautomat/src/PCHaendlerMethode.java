import java.util.Scanner;

public class PCHaendlerMethode {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen

		String t = nameArtikel();


		int p = anzahlArtikel();


		double n = nettoArtikel();


		double mwst = mwstArtikel();

		// Verarbeiten
		double gnp = berechneGesamtnettopreis(p, n);
		//double nettogesamtpreis = p * n;
		//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		double gbp = berechneGesamtbruttopreis(gnp, 1, mwst, 100);

		// Ausgeben
		// FORMELN
		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", t, p, gnp);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", t, p, gbp, mwst, "%");
		rechnungAusgeben(t, p, gnp, gbp, mwst);
	}
public static String nameArtikel () {
	Scanner myScanner = new Scanner (System.in);
	System.out.println("Welches Produkt moechten Sie erwerben:");
	String a = myScanner.next();
	return a;
}
public static int anzahlArtikel () {
	Scanner myScanner  = new Scanner (System.in);
	System.out.println("Geben Sie die Anzahl ein:");
	int a = myScanner.nextInt();
	return a;
}
public static double nettoArtikel () {
	Scanner myScanner = new Scanner (System.in);
	System.out.println("Geben Sie den Nettopreis ein:");
	double a = myScanner.nextDouble();
	return a;
}
public static double mwstArtikel () {
	Scanner myScanner = new Scanner (System.in);
	System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	double a = myScanner.nextDouble();
	return a;
}
public static double berechneGesamtnettopreis (double a, double b) {
	double gnp = a * b;
	return gnp;
}
public static double berechneGesamtbruttopreis (double a, double b, double c, double d) {
	double gbp = a * (b + c / d);
	return gbp;
}
public static void rechnungAusgeben (String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
	System.out.println("\tRechnung");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
}
}
