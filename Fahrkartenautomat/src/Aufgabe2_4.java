import java.util.Scanner;													
class Aufgabe2_4											
{
    public static void main(String[] args)									
    {
       Scanner tastatur = new Scanner(System.in);							
      
       double zuZahlenderBetrag; 											
       double eingezahlterGesamtbetrag;										
       double eingeworfeneM�nze;											
       double r�ckgabebetrag;												

       System.out.print("Zu zahlender Betrag (in EURO): ");					
       zuZahlenderBetrag = tastatur.nextDouble();							

       System.out.println(" ");
       //  Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;										
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)					
       {
    	   System.out.printf("Noch zu zahlen sind: " + "%.2f EURO \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));		
    	   System.out.println(" ");
    	   System.out.print("Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 2 Euro [Eingabe = 2]): ");						
    	   eingeworfeneM�nze = tastatur.nextDouble();																		
           eingezahlterGesamtbetrag += eingeworfeneM�nze;																	
       }

       //  Fahrscheinausgabe																					
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben:");													
       for (int i = 0; i < 8; i++)																			
       {
          System.out.print("=");																			
          try {
			Thread.sleep(250);																				
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");																			

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;										
       if(r�ckgabebetrag > 0.0)																				
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von: \n" + "%.2f EURO", r�ckgabebetrag);			
    	   System.out.println("\n");																		
    	   System.out.println("Wird in folgenden M�nzen ausgezahlt: \n");									
    	       	   
           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen													
           {
        	  System.out.println("2,00 EURO");																
	          r�ckgabebetrag -= 2.0;																		
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen													
           {
        	  System.out.println("1,00 EURO");																
	          r�ckgabebetrag -= 1.0;																		
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen													
           {
        	  System.out.println("0,50 CENT");																
	          r�ckgabebetrag -= 0.5;																		
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen													
           {
        	  System.out.println("0,20 CENT");																
 	          r�ckgabebetrag -= 0.2;																		
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen													
           {
        	  System.out.println("0,10 CENT");																
	          r�ckgabebetrag -= 0.1;																		
           }
           while(r�ckgabebetrag >= 0.05) // 5 CENT-M�nzen													
           {
        	  System.out.println("0,05 CENT");																
 	          r�ckgabebetrag -= 0.05;																		
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+										
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}