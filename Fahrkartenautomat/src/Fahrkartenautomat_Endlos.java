import java.util.Scanner;

class Fahrkartenautomat_Endlos {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		String option;
		Scanner op = new Scanner(System.in);

		do {
			
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			
			fahrkartenAusgeben();

			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n");

			
			System.out.println("Wollen Sie einen erneuten Durchlauf starten? (J/N)");
			option = op.next();

		} while (option.contains("J") || option.contains("j"));

		System.out.println("Programm beendet.");
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double[] ticketPreise = { 0, 2.90, 8.60, 23.50 };
		double gesamtpreis = 0.0;
		byte ticketAuswahl;
		byte ticketAnzahl = 0;

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("");

		do {
			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
			System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("Bezahlen (9)");

			
			System.out.print("Ihre Wahl: ");
			ticketAuswahl = tastatur.nextByte();

			
			while (ticketAuswahl != 9 && (ticketAuswahl >= 4 || ticketAuswahl <= 0)) {
				System.out.println(" >>falsche Eingabe<< ");
				System.out.print("Ihre Wahl: ");
				ticketAuswahl = tastatur.nextByte();
			}

			
			if (ticketAuswahl != 9) {
				System.out.print("Anzahl der Tickets: ");
				ticketAnzahl = tastatur.nextByte();
			}

			
			while (ticketAnzahl > 10 || ticketAnzahl <= 0) {
				System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
				System.out.print("Anzahl der Tickets: ");
				ticketAnzahl = tastatur.nextByte();
			}

			if (ticketAuswahl != 9) {
				gesamtpreis += ticketPreise[ticketAuswahl] * ticketAnzahl;
				System.out.printf("Zwischensumme: %.2f �\n", gesamtpreis);
			}
			
			System.out.println("");

		} while (ticketAuswahl != 9);

		return gesamtpreis;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		String einheit = "Euro";
		muenzeAusgeben(r�ckgabebetrag, einheit);

	}

	
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	public static void muenzeAusgeben(double betrag, String einheit) {
		if (betrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f %s \n", betrag, einheit);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (betrag >= 2.0) 
			{
				System.out.printf("2 %s \n", einheit);
				betrag -= 2.0;
			}
			while (betrag >= 1.0) 
			{
				System.out.printf("1 %s \n", einheit);
				betrag -= 1.0;
			}
			while (betrag >= 0.5) 
			{
				System.out.printf("0.50 %s \n", einheit);
				betrag -= 0.5;
			}
			while (betrag >= 0.2) 
			{
				System.out.printf("0.20 %s \n", einheit);
				betrag -= 0.2;
			}
			while (betrag >= 0.1) 
			{
				System.out.printf("0.10 %s \n", einheit);
				betrag -= 0.1;
			}
			while (betrag >= 0.05)
			{
				System.out.printf("0.05 %s \n", einheit);
				betrag -= 0.05;
			}
		}
	}
}