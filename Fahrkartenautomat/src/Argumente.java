public class Argumente {
	public static void main(String[] args) {
		ausgabe(1, "Mana");										// Festlegung Nummer, Festlegung Name 
		ausgabe(2, "Elise");									// Festlegung Nummer, Festlegung Name 
		ausgabe(3, "Johanna");									// Festlegung Nummer, Festlegung Name  
		ausgabe(4, "Felizitas");								// Festlegung Nummer, Festlegung Name 
		ausgabe(5, "Karla");									// Festlegung Nummer, Festlegung Name 
		System.out.println(vergleichen(1, 2)); 			 		// Ausgabe Text (Vergleich zwischen 1 und 2)
		System.out.println(vergleichen(1, 5));					// Ausgabe Text (Vergleich zwischen 1 und 5)
		System.out.println(vergleichen(3, 4));					// Ausgabe Text (Vergleich zwischen 3 und 4)
	}	
	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);					// Ausgabe von jeweiliger Zahl, Doppelpunkt und jeweiliger Name 
	}
	public static boolean vergleichen(int arg1, int arg2) {		// Boolsche Abfrage von int arg1 und arg2
		return (arg1 + 8) < (arg2 * 3);							// arg1 = 1		und		arg2 = 2		->		1+8 < 2*3? = false
	}
}