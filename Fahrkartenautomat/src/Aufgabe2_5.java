import java.util.Scanner;													// Scanner utility wird importiert

class Aufgabe2_5							// Name der Klasse
{
    public static void main(String[] args)									
    {
       Scanner tastatur = new Scanner(System.in);							
      
       double zuZahlenderBetrag; 											
       double eingezahlterGesamtbetrag;										
       double eingeworfeneM�nze;											
       double r�ckgabebetrag;												
       double preisFahrkarte;												
       byte anzahlFahrkarten;												
       
       System.out.println("Guten Tag, willkommen bei der BVG! ");			
       
       System.out.print("Bitte geben Sie einen Preis f�r das Ticket ein, welches Sie kaufen m�chten (Bitte nur in 5 CENT [0,05] Schritten): "); 		// Nachfrage nach Preis des Tickets und springe in n�chste Zeile
       preisFahrkarte = tastatur.nextDouble();
       
       System.out.println("");												
       
       System.out.print("Wie viele Fahrkarten m�chten Sie kaufen? (maximal 127 Tickets): ");		
       anzahlFahrkarten= tastatur.nextByte();														
       
       System.out.println(" ");												
       
       zuZahlenderBetrag = preisFahrkarte * anzahlFahrkarten;				 

       System.out.printf("Die von Ihnen eingegebene Anzahl an Fahrkarten kostet insgesamt: " + "%.2f EURO \n", zuZahlenderBetrag);		 
       
       System.out.println(" ");												
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;									
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)				
       {
    	   System.out.printf("Es sind noch " + "%.2f EURO zu zahlen! \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));		
    	   System.out.println(" ");
    	   System.out.print("Eingabe (mind. 5Ct [Eingabe = 0,05], h�chstens 10 Euro [Eingabe = 10]): ");						
    	   eingeworfeneM�nze = tastatur.nextDouble();																			
           eingezahlterGesamtbetrag += eingeworfeneM�nze;																		
       }

       // Fahrscheinausgabe																					
       // -----------------
       System.out.println("\nFahrschein wird gedruckt:");													
       for (int i = 0; i < 8; i++)																			
       {
          System.out.print("=");																			
          try {
			Thread.sleep(250);																				
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");																			

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;										   
       if(r�ckgabebetrag > 0.0)																				
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von: \n" + "%.2f EURO", r�ckgabebetrag);			
    	   System.out.println("\n");																		
    	   System.out.println("Dieser wird in folgenden M�nzen ausgezahlt: \n");									
    	       	   
           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen													
           {
        	  System.out.println("2,00 EURO");																
	          r�ckgabebetrag -= 2.0;																		
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen													
           {
        	  System.out.println("1,00 EURO");																
	          r�ckgabebetrag -= 1.0;																		
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen													
           {
        	  System.out.println("0,50 CENT");																
	          r�ckgabebetrag -= 0.5;																		
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen													
           {
        	  System.out.println("0,20 CENT");																
 	          r�ckgabebetrag -= 0.2;																		
           }
           while(r�ckgabebetrag >= 0.1) //  10 CENT-M�nzen													
           {
        	  System.out.println("0,10 CENT");																
	          r�ckgabebetrag -= 0.1;																		
           }
           while(r�ckgabebetrag >= 0.05) // 5  CENT-M�nzen													
           {
        	  System.out.println("0,05 CENT");																
 	          r�ckgabebetrag -= 0.05;																		 
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+								
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
    }
}
